/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.rwx.padlock.testapp;

import java.util.Map;
import static org.assertj.core.api.Assertions.assertThat;

import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.NewCookie;
import org.junit.Test;

/**
 *
 * @author mlvtito
 */
public class AsyncIT {
    
    private WebTarget client = ClientBuilder.newClient().target("http://localhost:8080/test");
    
    @Test
    public void should_HaveGetBean_when_Login_having_AsyncService() {
        Form form = new Form()
                .param("login", "my-wisely-choose-login")
                .param("password", "my-wisely-choose-login");

        Map<String, NewCookie> cookies = client.path("api/async/login")
                .request(MediaType.APPLICATION_JSON).post(Entity.form(form)).getCookies();

        assertThat(cookies).containsKeys("JTOKEN");
    }
}
