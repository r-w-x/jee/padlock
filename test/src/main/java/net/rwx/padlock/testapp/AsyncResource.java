/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.rwx.padlock.testapp;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import jakarta.enterprise.inject.spi.Bean;
import jakarta.enterprise.inject.spi.BeanManager;
import jakarta.inject.Inject;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.container.AsyncResponse;
import jakarta.ws.rs.container.Suspended;
import jakarta.ws.rs.core.Response;
import net.rwx.padlock.PadlockSession;
import net.rwx.padlock.PasswordService;
import net.rwx.padlock.annotations.WithoutAuthentication;

/**
 *
 * @author mlvtito
 */
@Path("async")
public class AsyncResource {

    private static final Logger logger = Logger.getLogger(AsyncResource.class.getName());
    
    @Inject
    private PasswordService passwordService;

    @Inject
    private PadlockSession session;

    @Inject
    private BeanManager beanManager;
    
    @POST
    @Path("login")
    @WithoutAuthentication
    public void login(@FormParam("login") String login, @FormParam("password") String password, @Suspended AsyncResponse asyncResponse) {
        ExecutorService es = Executors.newSingleThreadExecutor();
        PadlockSession mySession = getBeanInstanceFromContext();
        es.submit(() -> {
            try {
                String value = process(login, password, mySession);
                asyncResponse.resume(Response.accepted(value).build());
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Error while processing async response", e);
                asyncResponse.resume(e);
            } finally {
                es.shutdown();
            }
        });
    }

    private PadlockSession getBeanInstanceFromContext() {
        Bean<PadlockSession> bean = (Bean<PadlockSession>) beanManager.resolve(beanManager.getBeans(PadlockSession.class));
        return beanManager.getContext(bean.getScope()).get(bean, beanManager.createCreationalContext(bean));
    }
    
    private String process(String login, String password, PadlockSession mySession) {
        String hash = passwordService.hash(login.toCharArray(), true);
        if (passwordService.verify(password.toCharArray(), hash, true)) {
            mySession.setAttribute("user", TestUserBean.builder().name("John Doe").mail("john.doe@test.net").build());
            mySession.setAuthenticated(true);
            return "connected";
        } else {
            return "not connected";
        }
    }
}
