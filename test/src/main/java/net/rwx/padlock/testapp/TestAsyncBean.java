/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.rwx.padlock.testapp;

/**
 *
 * @author mlvtito
 */
public class TestAsyncBean {
    private AsyncResource asyncResource;
    private String login, password;

    public TestAsyncBean(AsyncResource asyncResource, String login, String password) {
        this.asyncResource = asyncResource;
        this.login = login;
        this.password = password;
    }

    public AsyncResource getAsyncResource() {
        return asyncResource;
    }

    public void setAsyncResource(AsyncResource asyncResource) {
        this.asyncResource = asyncResource;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
