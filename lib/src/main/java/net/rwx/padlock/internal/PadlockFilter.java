/*
 * Copyright 2017 Arnaud Fonce <arnaud.fonce@r-w-x.net>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.rwx.padlock.internal;

import java.util.concurrent.ConcurrentHashMap;

import jakarta.enterprise.inject.spi.Bean;
import jakarta.enterprise.inject.spi.BeanManager;
import jakarta.inject.Inject;
import jakarta.ws.rs.container.*;
import jakarta.ws.rs.core.*;
import jakarta.ws.rs.ext.Provider;
import jakarta.ws.rs.ext.RuntimeDelegate;
import net.rwx.padlock.annotations.WithoutAuthentication;
import net.rwx.padlock.PadlockSession;

/**
 *
 * @author <a href="mailto:arnaud.fonce@r-w-x.net">Arnaud Fonce</a>
 */
@Provider
class PadlockFilter implements ContainerRequestFilter, ContainerResponseFilter {

    private static final String JWT_COOKIE_NAME = "JTOKEN";

    private static final ConcurrentHashMap<ContainerRequestContext, PadlockSession> sessionsMap = new ConcurrentHashMap<>();

    @Inject
    private PadlockSession session;

    @Context
    private ResourceInfo resourceInfo;

    @Inject
    private BeanManager beanManager;

    @Inject
    private TokenHelper tokenHelper;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        try {
            readTokenCookie(requestContext);
            if (needAuthentication()) {
                if (session.isAuthenticated()) {
                    checkAuthorization(requestContext);
                    sessionsMap.put(requestContext, getBeanInstanceFromContext());
                } else {
                    throw new UnauthorizedException();
                }
            } else {
                sessionsMap.put(requestContext, getBeanInstanceFromContext());
            }
        } catch (UnauthorizedException ue) {
            unauthorized(requestContext);
        } catch( BadTokenException bte ) {
            unauthorizedWithCookieRemove(requestContext);
        }
    }

    private boolean needAuthentication() {
        return !resourceInfo.getResourceMethod().isAnnotationPresent(WithoutAuthentication.class);
    }

    private void readTokenCookie(ContainerRequestContext requestContext) throws BadTokenException {
        Cookie tokenCookie = requestContext.getCookies().get(JWT_COOKIE_NAME);
        if (tokenCookie != null) {
            tokenHelper.parseTokenAndExtractBean(session, tokenCookie.getValue());
        }
    }

    private void checkAuthorization(ContainerRequestContext requestContext) throws UnauthorizedException {
        AuthorizationChecker authChecker = AuthorizationChecker.builder()
                .fromAuthorizedMethod(resourceInfo.getResourceMethod())
                .valueFrom(requestContext)
                .withBeanManager(beanManager)
                .build();

        authChecker.check();
    }

    private void unauthorized(ContainerRequestContext requestContext) {
        requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                .build()
        );
    }
    
    private void unauthorizedWithCookieRemove(ContainerRequestContext requestContext) {
        requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                .header(HttpHeaders.SET_COOKIE, buildCookieHeaderToRemoveToken())
                .build()
        );
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        PadlockSession sessionFromMap = sessionsMap.remove(requestContext);
        if (sessionFromMap != null && !sessionFromMap.isValid()) {
            responseContext.getHeaders().add(HttpHeaders.SET_COOKIE, buildCookieHeaderToRemoveToken());
        }else if (sessionFromMap != null && !sessionFromMap.isEmpty()) {
            String token = tokenHelper.serializeBeanAndCreateToken(sessionFromMap);
            NewCookie cookie = RuntimeDelegate.getInstance()
                    .createHeaderDelegate(NewCookie.class)
                    .fromString(JWT_COOKIE_NAME + "=" + token + ";Secure;HttpOnly;Path=/");
            responseContext.getHeaders().add(HttpHeaders.SET_COOKIE, cookie);
        }
    }
    
    private NewCookie buildCookieHeaderToRemoveToken() {
        return RuntimeDelegate.getInstance()
                .createHeaderDelegate(NewCookie.class)
                .fromString(JWT_COOKIE_NAME + "=deleted;Secure;HttpOnly;Path=/;expires=Thu, 01 Jan 1970 00:00:00 GMT");
    }

    private PadlockSession getBeanInstanceFromContext() {
        Bean<PadlockSession> bean = (Bean<PadlockSession>) beanManager.resolve(beanManager.getBeans(PadlockSession.class));
        return beanManager.getContext(bean.getScope()).get(bean, beanManager.createCreationalContext(bean));
    }
}
